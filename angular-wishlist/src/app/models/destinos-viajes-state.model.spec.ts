import {
    reducerDestinosViajes,
    DestinosViajesState,
    initializeDestinosViajesState,
    InitMyDataAction,
    NuevoDestinoAction
  } from './destinos-viajes-state.model';
  import { DestinoViaje } from './destino-viaje.model';
  
  describe('reducerDestinosViajes', () => {
    it('should reduce init data', () => {
      // setup
      const prevState: DestinosViajesState = initializeDestinosViajesState();
      const action: InitMyDataAction = new InitMyDataAction(['Salta', 'Jujuy']);
      // action
      const newState: DestinosViajesState = reducerDestinosViajes(prevState, action);
      // assertions
      expect(newState.items.length).toEqual(2);
      expect(newState.items[0].nombre).toEqual('Salta');
      expect(newState.items[1].nombre).toEqual('Jujuy');
      // tear down
    });
  
    it('should reduce new item added', () => {
      const prevState: DestinosViajesState = initializeDestinosViajesState();
      const action: NuevoDestinoAction = new NuevoDestinoAction(new DestinoViaje('Barcelona', 'url'));
      const newState: DestinosViajesState = reducerDestinosViajes(prevState, action);
      expect(newState.items.length).toEqual(1);
      expect(newState.items[0].nombre).toEqual('Barcelona');
      expect(newState.items[0].urlImagen).toEqual('url');
    });
  });
  